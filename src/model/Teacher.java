package model;

public class Teacher {

	private String name;
	private String password;
	
	public Teacher(){}
	
	public Teacher(String name, String pssword){
		this.name = name;
		this.password = pssword;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
