package control;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import model.Student;
import utils.JDBCUtil;
import utils.MD5Util;
/**
 * 数据库操作类
 */
public class JDBC{
	
	JDBCUtil jdbc = new JDBCUtil();
	MD5Util md5 = new MD5Util();
	static Connection conn = null;	//创建mysql连接对象
	static PreparedStatement ps = null;	//创建执行mysql命令对象
	static ResultSet rs = null;	//创建mysql返回结果集对象
	
	/**
	 * 添加基本学生信息记录
	 * @param id
	 * @param name
	 * @param sex
	 * @param age
	 * @param password
	 */
	public void add(String id, String name, String sex, int age, String password) {
		password = md5.getMD5(password);//加密密码
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			String sql = "insert into students values(?,?,?,?,?,?)";
			//通过Connection对象获取PreparedStatement对象
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, name);
			ps.setString(3, sex);
			ps.setInt(4, age);
			ps.setString(5, null);
			ps.setString(6, password);
			ps.executeUpdate();//执行更新sql语句
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
	}
	
	/**
	 * 设置学生分数
	 * @param id
	 * @param mark
	 */
	public void setMark(String id, int mark) {
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			//修改数据库表中字段为id的mark字段记录
			String sql = "update students set mark=? where id=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, mark);
			ps.setString(2, id);
			//执行更新sql语句
			ps.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
	}
	
	/**
	 * 删除学生信息记录
	 * @param id
	 */
	public void delete(String id) {
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			String sql = "delete from students where id = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.executeUpdate();//执行sql语句
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
	}
	
	/**
	 * 修改学生基本信息
	 * @param id
	 * @param name
	 * @param sex
	 * @param age
	 */
	public void alter(String id, String name, String sex, int age) {
		//先删除该ID学生信息
		delete(id);
		//最后调用添加学生信息方法
		add(id, name, sex, age, id);
	}
	
	/**
	 * 修改学生登录密码
	 * @param name
	 * @param password
	 */
	public void alterPass(String name, String password) {
		password = md5.getMD5(password);//加密密码
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			String sql = "update students set password=? where name=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, password);
			ps.setString(2, name);
			ps.executeUpdate();//执行更新sql语句
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
	}
	
	/**
	 * 查看学生信息记录，并以集合方式返回
	 * @return ArrayList<Student>
	 */
	public ArrayList<Student> select() {
		ArrayList<Student> stuList = new ArrayList<Student>();
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			//查询学生数据表所有信息语句
			String sql = "select * from students order by id";
			//通过Connection对象获取PrepareStatement对象
			ps = conn.prepareStatement(sql);
			//执行查询sql语句，并返回结果集
			rs = ps.executeQuery(sql);
			while(rs.next()) {
				Student stu = new Student();//创建临时学生信息对象
				stu.setStuId(rs.getString("id"));
				stu.setStuName(rs.getString("name"));
				stu.setStuSex(rs.getString("sex"));
				stu.setStuAge(rs.getInt("age"));
				if(rs.getString("mark") != null) {
					stu.setStuMark(Integer.parseInt(rs.getString("mark")));
				}
				stu.setStuPass(rs.getString("password"));
				stuList.add(stu);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
		return stuList;
	}
	
	/**
	 * 判断学生姓名是否已有记录
	 * @param name
	 * @return boolean
	 */
	public boolean ishavaName(String name) {
		boolean flag = false;// 正/误 标识
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			String sql = "select name from students";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			//遍历查询结果
			while(rs.next()) {
				//获取数据库name字段信息，并转为整型
				String jname = rs.getString("name");
				if(name.equals(jname)) {
					flag = true;
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
		return flag;
	}
	
	/**
	 * 判断学生ID在数据库中是否已有
	 * @param id
	 * @return boolean
	 */
	public boolean ishavaId(String id) {
		boolean flag = false;// 正/误 标识
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			String sql = "select id from students";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			//遍历查询结果
			while(rs.next()) {
				//获取数据库id字段信息，并转为整型
				String jid = rs.getString("id");
				if(id.equals(jid)) {
					flag = true;
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
		return flag;
	}
	
	/**
	 * 判断学生姓名，除自己以外，在数据库中是否已有
	 * @param id
	 * @param name
	 * @return boolean
	 */
	public boolean alterIshavaName(String id,String name) {
		boolean flag = false;
		try {
			conn = JDBCUtil.getConnection();//调用数据库工具类，获取连接
			String sql = "select * from students";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			//遍历查询结果
			while(rs.next()) {
				//获取数据库name字段信息，并转为整型
				String jid = rs.getString("id");
				String jname = rs.getString("name");
				if(id.equals(jid)) {
					continue;
				}
				if(name.equals(jname)) {
					flag = true;
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.release(ps, conn);//调用数据库工具类，释放资源
		}
		return flag;
	}
	
//	public static void main(String[] args) {
//		
//		ArrayList<Student> stuList = new JDBC().select();
//		System.out.println("[id	|name	|sex	|age	|mark	]");
//		for (Student stu : stuList) {
//			String id = stu.getStuId();
//			String name = stu.getStuName();
//			String sex = stu.getStuSex();
//			int age = stu.getStuAge();
//			int mark = stu.getStuMark();
//			System.out.println("["+id+"	|"+name+"	|"+sex+"	|"+age+"	|"+mark+"	]");
//		}
//	}
}